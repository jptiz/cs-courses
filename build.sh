#!/usr/bin/env bash
DEFAULT="\033[0m"

GREEN="\033[92m"
RED="\033[91m"
YELLOW="\033[93m"
OTHER="\033[90m"

INFO="[${YELLOW}INFO${DEFAULT}]"
ERROR="[${RED}ERROR${DEFAULT}]"
GOOD="[${GREEN}GOOD${DEFAULT}]"
SKIP="[${OTHER}SKIP${DEFAULT}]"

shopt -s extglob

FUNCTIONS="build|clean|serve"

MDBOOK=$(command -v mdbook-mermaid)
if [ ${MDBOOK} ]
then
    echo "mdbook-mermaid found."
else
    echo "Adding mdbook-mermaid to path..."
    MDBOOK="${CARGO_HOME}/bin/mdbook-mermaid"
fi

build-book() {
    local book=$1

    echo "[INFO] Building ${book}..."
    ${MDBOOK} "${book}"
}

fix-js-css-path() {
    if [ "$1" = "--css" ]
    then
        local ext="css"
        local tag="href"
        shift
    else
        local ext="js"
        local tag="src"
    fi

    local pat="${tag}=\"\(.*\)\(special-.*.${ext}\)\""

    if [ "$1" = "--local" ]
    then
        BASEPATH="$(pwd)/book"
        local dst="${tag}=\"${BASEPATH//\//\\/}\/${ext}\/\2\""
        echo "Fixing ${ext} paths (local build)..."
        echo "    -> pat: ${pat}"
        echo "    -> dst: ${dst}"
    else
        local dst="${tag}=\"\/${ext}\/\2\""
        echo "Fixing ${ext} paths..."
    fi

    for file in $(find -name "*.html");
    do
        sed -i "s/${pat}/${dst}/g" "${file}"
    done
}

nofun() {
    if [ -z $1 ]; then
        echo "Usage: ${0} <function>"
    else
        echo "No function \"${1}\"."
    fi

    echo "Available functions: "
    echo "    $(echo ${FUNCTIONS} | sed 's/|/\n    /g')"
    exit 1
}

# Commands

build() {
    echo "Build flags: $*"

    for book in main
    do
        build-book "${book}"
    done

    fix-js-css-path $1
    fix-js-css-path --css $1
}

clean() {
    rm -rf book
}

FS="@(${FUNCTIONS})"
case $1 in
    ${FS})
        $*
        ;;
    *)
        nofun $*
        ;;
esac
