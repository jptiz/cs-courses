Sobre estes textos
==================


Sobre o que NÃO SÃO estes textos
--------------------------------

Ao ler os textos que estão escritos/escreverei aqui, é importante ter em mente
que:

- Eles **não são** uma _rant_ (como as do Linus). O objetivo deles não é expor
  raiva ou simplesmente desabafar;
- Eles não são para criar uma corrente de ódio às univesidades;
- Eles não são para diminuir a competência dos professores e pesquisadores;
- Eles não são para afastar os interessados em entrar em cursos de Ciência da
  Computação ou cursos que envolvam conceitos de Computação (incluindo
  programação).

É claro, alguns tópicos podem tocar em pontos com os quais tenho sentimentos
bastante fortes e isso acabar me fazendo escrever de maneira mais agressiva,
mas tomarei o devido cuidado. **Se em algum momento você sentir que um texto se
tornou desnecessariamente agressivo, entre em contato!** (Aceito que seja
abrindo uma issue.)


Sobre o que SÃO estes textos
----------------------------

Meu objetivo aqui é:
- Expor problemas (que de fato existem!) comuns a cursos de programação e de
  computação nas universidades brasileiras (não significa que lá fora seja
  diferente, mas que tenho mais conhecimento do ensino brasileiro -- afinal é
  onde vivo);
- Demonstrar por que eles são problemas;
- Propor caminhos/ideias para mitigar e evitar esses problemas.

Algo que também preciso deixar claro é que, por mais que os problemas que vou
citar recaiam em mais responsabilidades para os professores, reconheço que
estes precisam lidar, além de várias outras coisas, com pesquisa e burocracias,
e portanto prazos e incomodações diárias que vão acabando com a saúde mental
desses profissionais. Mas peço que reconheçam que problemas quanto ao ensino
**não podem** ser deixados de lado, deve haver uma mudança gradual e conjunta.
Talvez parte das ideias fiquem mais claras no texto que pretendo escrever a
seguinte ("Do que se trata uma graduação").

Também reconheço que **deve** haver apoio dos alunos, e deixo uma mensagem aos
professores: há alunos que **adorariam** ajudá-los a melhorar a qualidade do
ensino, então atuem em conjunto. Não existe "professores aqui e alunos lá":
somos parte de uma coisa só.
